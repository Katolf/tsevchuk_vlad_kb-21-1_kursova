CREATE DATABASE Restaurant;

USE Restaurant;

CREATE TABLE Kliyenty (
    ID_Kliyenta INT IDENTITY (1,1) PRIMARY KEY,
    Imya VARCHAR(100),
    Kontakti_Dani VARCHAR(100)
);

CREATE TABLE Personal (
    ID_Personalu INT IDENTITY (1,1) PRIMARY KEY,
    Imya VARCHAR(100),
    Posada VARCHAR(50),
);

CREATE TABLE Menyu (
    ID_Stravy INT IDENTITY (1,1) PRIMARY KEY,
    Nazva_Stravy VARCHAR(100),
    Opys VARCHAR(200),
    Tsina DECIMAL(10, 2));

CREATE TABLE Zamovlenni_Stravy (
    ID_Zamovlennya INT IDENTITY (1,1) PRIMARY KEY,
    ID_Kliyenta INT,
    ID_Stravy INT,
    ID_Personalu INT,
    Data_Zamovlennya DATETIME,
    Zahalna_Suma DECIMAL(10, 2),
    Kilkist INT,
    FOREIGN KEY (ID_Kliyenta) REFERENCES Kliyenty(ID_Kliyenta),
    FOREIGN KEY (ID_Stravy) REFERENCES Menyu(ID_Stravy),
    FOREIGN KEY (ID_Personalu) REFERENCES Personal(ID_Personalu)
);


	
------------------------
CREATE TABLE Postavki (
    ID_Postavki INT IDENTITY (1,1) PRIMARY KEY,
    Nazva_Postachalnyka VARCHAR(100),
    Kilkist INT
);

CREATE TABLE Inhrediyentu (
    ID_Inhrediyenta INT IDENTITY (1,1) PRIMARY KEY,
    Nazva_Inhrediyenta VARCHAR(100),
    Odynytsi_Vymiru VARCHAR(50),
	ID_Stravy INT,
	ID_Postavki INT,
	Tsina DECIMAL(10, 2)
	FOREIGN KEY (ID_Stravy) REFERENCES Menyu(ID_Stravy),
	FOREIGN KEY (ID_Postavki) REFERENCES Postavki(ID_Postavki)
);

CREATE TABLE Sklad (
    ID_Inhrediyenta INT,
	Kilkist INT,
	FOREIGN KEY (ID_Inhrediyenta) REFERENCES Inhrediyentu(ID_Inhrediyenta)
);

CREATE TABLE Inhrediyentu_Stravu (
	ID_Inhrediyenta INT,
	ID_Stravy int
	FOREIGN KEY (ID_Inhrediyenta) REFERENCES Inhrediyentu(ID_Inhrediyenta),
	FOREIGN KEY (ID_Stravy) REFERENCES Menyu(ID_Stravy)
);
---------------------


CREATE TABLE Bronyuvannya (
    ID_Bronyuvannya INT IDENTITY (1,1) PRIMARY KEY,
    ID_Kliyenta INT,
    Data_Bronyuvannya DATETIME,
    Kilkist_Osib INT,
    FOREIGN KEY (ID_Kliyenta) REFERENCES Kliyenty(ID_Kliyenta)
);

-- ���������� ������� �볺���
INSERT INTO Kliyenty (Imya, Kontakti_Dani)
VALUES ('����', '+380501234567'),
       ('���������', '+380501234568'),
       ('����', '+380501234569'),
       ('�����', '+380501234570'),
       ('�����', '+380501234571'),
       ('������', '+380501234572'),
       ('������', '+380501234573'),
       ('���', '+380501234574'),
       ('�����', '+380501234575'),
       ('����', '+380501234576');

-- ���������� ������� ��������
INSERT INTO Personal (Imya, Posada)
VALUES ('�����', '��������'),
       ('�����', '��������'),
       ('������', '������'),
       ('����', '�����'),
       ('����', '�����'),
       ('���������', '��������'),
       ('������', '��������');

-- ���������� ������� ����
INSERT INTO Menyu (Nazva_Stravy, Opys, Tsina)
VALUES ('����', '����������� ���������� ����', 100.00),
       ('��������', '�������� � ���������', 80.00),
       ('�������', '������� � �����', 120.00),
       ('������', '������ � ��������', 70.00),
       ('�����', '����� � �����', 130.00),
       ('�������', '������� � �����', 110.00),
       ('������', '������ � �����', 60.00),
       ('������', '������ � �������', 50.00),
       ('������', '������ � �������', 40.00),
       ('�������', '������� � �������', 70.00);


-- ���������� ������� ��������� ������
INSERT INTO Zamovlenni_Stravy (ID_Kliyenta, ID_Stravy, ID_Personalu, Data_Zamovlennya, Zahalna_Suma, Kilkist)
VALUES (1, 1, 1, '2024-01-01 19:00:00', 200.00, 2),
       ( 2, 2, 2, '2024-01-02 19:00:00', 180.00, 2),
       ( 3, 3, 3, '2024-01-03 19:00:00', 240.00, 2),
       ( 4, 4, 4, '2024-01-04 19:00:00', 210.00, 2),
       ( 5, 5, 5, '2024-01-05 19:00:00', 220.00, 2),
       ( 6, 6, 6, '2024-01-06 19:00:00', 230.00, 2),
       ( 7, 7, 7, '2024-01-07 19:00:00', 240.00, 2),
       ( 8, 8, 1, '2024-01-08 19:00:00', 250.00, 2),
       ( 9, 9, 2, '2024-01-09 19:00:00', 260.00, 2),
       ( 10, 10, 3, '2024-01-10 19:00:00', 270.00, 2);



----------------------------------------
-- ���������� ������� ��������
INSERT INTO Postavki (Nazva_Postachalnyka, Kilkist)
VALUES ('���������', 100),
	   ('�����������', 200),
	   ('�����������', 300),
	   ('�����������', 100),
	   ('���������', 200),
	   ('������������', 400),
	   ('�������������', 100),
	   ('�����', 50),
	   ('˳����', 300),
	   ('����������', 100),
	   ('�����˳��', 200),
	   ('���', 200),
	   ('ѳ����', 100),
	   ('������', 80),
	   ('�����', 150),
	   ('������', 200);


-- ��������� �����䳺��� ��� ������ "����"   --------------------------------------------------------------------
INSERT INTO Inhrediyentu (Nazva_Inhrediyenta, Odynytsi_Vymiru, ID_Stravy, ID_Postavki, Tsina)
VALUES ('��� ������', '��', 1, 1, 20.00),
       ('���� (�������)', '��', 1, 2, 50.00),
       ('��������', '��', 1, 3, 10.00),

-- ��������� �����䳺��� ��� ������ "��������"
	   ('ҳ���', '��', 2, 4, 15.00),
       ('��������', '��', 2, 3, 5.00),
       ('������', '��', 2, 5, 8.00),

-- ��������� �����䳺��� ��� ������ "�������"
	   ('���� (���������)', '��', 3, 6, 60.00),
       ('�������', '��', 3, 7, 12.00),
       ('���', '��', 3, 8, 25.00),

-- �����䳺��� ��� ������ "������"
      ('��������', '��', 4, 3, 10.00),
      ('������', '��', 4, 9, 30.00),
      ('������', '��', 4, 5, 8.00),

-- �����䳺��� ��� ������ "�����"
      ('���� (�������)', '��', 5, 2, 50.00),
      ('���', '��', 5, 8, 20.00),

-- �����䳺��� ��� ������ "�������"
      ('������ ����', '��', 6, 10, 45.00),
      ('����', '��', 6, 11, 2.00),

-- �����䳺��� ��� ������ "������"
    ('ҳ���', '��', 6, 4, 20.00),
    ('����', '��', 6, 11, 15.00),
    ('���', '�', 6, 12, 25.00),

-- �����䳺��� ��� ������ "������"
    ('������', '�', 7, 11, 10.00),
    ('����', '��', 7, 11, 15.00),
    ('�����', '�', 7, 13, 20.00),

-- �����䳺��� ��� ������ "������"
    ('ҳ���', '��', 8, 4, 20.00),
    ('�����', '�', 8, 14, 30.00),
    ('�����', '��', 8, 15, 15.00),

-- �����䳺��� ��� ������ "�������" 
    ('������', '��', 9, 16, 35.00),
    ('����', '��', 9, 11, 15.00),
    ('�����', '�', 9, 13, 20.00);


-- ���������� ������� �����
INSERT INTO Sklad (ID_Inhrediyenta, Kilkist)
VALUES 
    (1, 200),  -- ��� ������
    (2, 150),  -- ���� (�������)
    (3, 190), -- ��������
    (4, 200),  -- ҳ���
    (5, 200),  -- ������
    (6, 90),  -- ���� (���������)
    (7, 140),  -- �������
    (8, 30),  -- ���
    (9, 50),  -- ������
    (10, 110), -- ������ ����
    (11, 100),-- ����
    (12, 50), -- ���
    (13, 40), -- ������
    (14, 60), -- �����
    (15, 60), -- �����
    (16, 80), -- �����
    (17, 69); -- ������


-- ���������� ������� ���.������
INSERT INTO Inhrediyentu_Stravu (ID_Inhrediyenta, ID_Stravy)
VALUES 
    (1, 1), -- ��� ������
    (2, 1), -- ���� (�������)
    (3, 1), -- ��������

-- �����䳺��� ��� ������ "��������"
    (4, 2), -- ҳ���
    (3, 2), -- ��������
    (5, 2), -- ������

-- �����䳺��� ��� ������ "�������"
    (6, 3), -- ���� (���������)
    (7, 3), -- �������
    (8, 3), -- ���

-- �����䳺��� ��� ������ "������"
    (3, 4), -- ��������
    (9, 4), -- ������
    (5, 4), -- ������

-- �����䳺��� ��� ������ "�����"
    (2, 5), -- ���� (�������)
    (8, 5), -- ���

-- �����䳺��� ��� ������ "�������"
    (10, 6), -- ������ ����
    (11, 6), -- ����

-- �����䳺��� ��� ������ "������"
    (4, 7),  -- ҳ���
    (11, 7), -- ����
    (12, 7), -- ���

-- �����䳺��� ��� ������ "������"
    (13, 8), -- ������
    (11, 8), -- ����
    (14, 8), -- �����

-- �����䳺��� ��� ������ "������"
    (4, 9),  -- �������� �������
    (15, 9), -- �����
    (16, 9), -- �����

-- �����䳺��� ��� ������ "�������"
    (17, 10), -- ������
    (11, 10), -- ����
    (14, 10); -- �����

-------------------------------------------


-- ���������� ������� ����������
INSERT INTO Bronyuvannya (ID_Kliyenta, Data_Bronyuvannya, Kilkist_Osib)
VALUES (1, '2024-01-01 19:00:00', 2),
       (2, '2024-01-02 19:00:00', 4),
       (3, '2024-01-03 19:00:00', 2),
       (4, '2024-01-04 19:00:00', 3),
       (5, '2024-01-05 19:00:00', 2),
       (6, '2024-01-06 19:00:00', 2),
       (7, '2024-01-07 19:00:00', 4),
       (8, '2024-01-08 19:00:00', 2),
       (9, '2024-01-09 19:00:00', 3),
       (10, '2024-01-10 19:00:00', 2);



SELECT * FROM Kliyenty;
SELECT * FROM Menyu;
SELECT * FROM Zamovlenni_Stravy;
SELECT * FROM Inhrediyentu;
SELECT * FROM Sklad;
SELECT * FROM Inhrediyentu_Stravu;
SELECT * FROM Postavki;
SELECT * FROM Personal;
SELECT * FROM Bronyuvannya;


------------------------------------------------SELECT-----------------------------------------------------

-- ������ ��� ��������� �������� ���������� ��� ����������, ��������� ���������� ��� �볺��� �� ������
SELECT Z.ID_Zamovlennya, K.Imya AS Kliyent_Imya, K.Kontakti_Dani AS Kliyent_Kontakt,
       M.Nazva_Stravy, M.Tsina, Z.Data_Zamovlennya, Z.Zahalna_Suma, Z.Kilkist
FROM Zamovlenni_Stravy Z
JOIN Kliyenty K ON Z.ID_Kliyenta = K.ID_Kliyenta
JOIN Menyu M ON Z.ID_Stravy = M.ID_Stravy;

-- ��������� �������� ������� �� ���� ��������� ��� ��� �볺���, �� �������� � ����� ����� ����:
SELECT Z.ID_Kliyenta, SUM(Z.Zahalna_Suma) AS Total_Sum, COUNT(Z.ID_Zamovlennya) AS Total_Orders
FROM Zamovlenni_Stravy Z
WHERE Z.Data_Zamovlennya BETWEEN '2024-01-01 00:00:00' AND '2024-01-10 23:59:59'
GROUP BY Z.ID_Kliyenta;


-- ϳ�������� ������� �������� � �����, �� ����������� 100 �������:
SELECT S.ID_Inhrediyenta, COUNT(S.ID_Inhrediyenta) AS Total_Quantity
FROM Sklad S
WHERE S.Kilkist > 100 -- ������ ����� ������� ��������
GROUP BY S.ID_Inhrediyenta;


-------------------------------���������------------------------------------

-- ��������� � �������� �� �����䳺����� ��� ������� ������:
CREATE PROC ingr 
as 
SELECT Z.ID_Zamovlennya, M.Nazva_Stravy AS Dish_Name, I.Nazva_Inhrediyenta AS Ingredient_Name, Z.Kilkist
FROM Zamovlenni_Stravy Z
INNER JOIN Menyu M ON Z.ID_Stravy = M.ID_Stravy
INNER JOIN Inhrediyentu I ON Z.ID_Stravy = I.ID_Stravy;

exec ingr



-- ��������� ��������� ���������� ��� ������:
CREATE PROCEDURE OnovlennyaStravy
    @ID_Stravy INT,
    @Nazva_Stravy VARCHAR(100),
    @Opys VARCHAR(200),
    @Tsina DECIMAL(10, 2)
AS
BEGIN
    UPDATE Menyu
    SET Nazva_Stravy = @Nazva_Stravy, Opys = @Opys, Tsina = @Tsina
    WHERE ID_Stravy = @ID_Stravy;
END;

EXEC OnovlennyaStravy 3, '�������', '������� ��������', 90.00;
Select * from Menyu


-- ��������� ������ �볺��� � �������

CREATE PROCEDURE DodatuKlienta
    @Imya VARCHAR(100),
    @Kontakti_Dani VARCHAR(100)
AS
BEGIN
    INSERT INTO Kliyenty (Imya, Kontakti_Dani)
    VALUES (@Imya, @Kontakti_Dani);
END;

EXEC DodatuKlienta '�����', '+0504826296';
SELECT * FROM Kliyenty



--------------------------�������---------------------------

-- ����� ��������� �������, ��� ����� 0:
CREATE TRIGGER PreventNegativeUpdate_Sklad
ON Sklad
INSTEAD OF UPDATE
AS
BEGIN
    IF EXISTS (SELECT 1 FROM inserted WHERE Kilkist < 0)
    BEGIN
        RAISERROR('��������� ������� ����� 0 ����������.', 16, 1);
    END
    ELSE
    BEGIN
        UPDATE Sklad
        SET Kilkist = i.Kilkist
        FROM Sklad s
        INNER JOIN inserted i ON s.ID_Inhrediyenta = i.ID_Inhrediyenta;
    END;
END;


update Sklad set Kilkist = -1 where ID_Inhrediyenta = 1

select * from Sklad

---------------------------��������� ������ ��� ������������----------------

create login AdminDB with password = 'adminpass' -- ��������� ����� ��� ������������� ���� �����
create login Director with password = 'directorpass' -- ��������� ����� ��� ���������
create login Manager with password = 'managerpass' -- ��������� ����� ��� ���������
create login Kyhar with password = 'kyharpass' -- ��������� ����� ��� ������
create login Oficiant with password ='oficiantpass' -- ��������� ����� ��� ���������
create login Barmen with password ='barmenpass' -- ��������� ����� ��� �������


-- ��������� ������������ �� ����� ��������� ������
create user AdminDB for login AdminDB
create user Director for login Director
create user Manager for login Manager
create user Kyhar for login Kyhar
create user Oficiant for login Oficiant
create user Barmen for login Barmen

--����������� ��� ��� ������������� ��
alter role db_owner add member AdminDB

--��������� �����
create role director1 authorization AdminDB
create role manager1 authorization AdminDB
create role kyhar1 authorization AdminDB
create role oficiant1 authorization AdminDB
create role barmen1 authorization AdminDB

--����������� ������������ �� �����
alter role director1 add member Director
alter role manager1 add member Manager
alter role kyhar1 add member Kyhar
alter role oficiant1 add member Oficiant
alter role barmen1 add member Barmen

-------------
--�������� ��������� ������������ �� �����
select rp.name as rolename, mp.name as username
FROM sys.database_role_members drm
JOIN sys.database_principals rp ON drm.role_principal_id = rp.principal_id
JOIN sys.database_principals mp ON drm.member_principal_id = mp.principal_id
WHERE rp.name = 'director1'
UNION
select rp.name as rolename, mp.name as username
FROM sys.database_role_members drm
JOIN sys.database_principals rp ON drm.role_principal_id = rp.principal_id
JOIN sys.database_principals mp ON drm.member_principal_id = mp.principal_id
WHERE rp.name = 'manager1'
UNION
select rp.name as rolename, mp.name as username
FROM sys.database_role_members drm
JOIN sys.database_principals rp ON drm.role_principal_id = rp.principal_id
JOIN sys.database_principals mp ON drm.member_principal_id = mp.principal_id
WHERE rp.name = 'kyhar1'
UNION
select rp.name as rolename, mp.name as username
FROM sys.database_role_members drm
JOIN sys.database_principals rp ON drm.role_principal_id = rp.principal_id
JOIN sys.database_principals mp ON drm.member_principal_id = mp.principal_id
WHERE rp.name = 'oficiant1'
UNION
select rp.name as rolename, mp.name as username
FROM sys.database_role_members drm
JOIN sys.database_principals rp ON drm.role_principal_id = rp.principal_id
JOIN sys.database_principals mp ON drm.member_principal_id = mp.principal_id
WHERE rp.name = 'barmen1'


----------������� ���� ��������� ������������----------------
--��������
grant select, insert, update, delete on Menyu to director1
grant select, insert, update, delete on Personal to director1
grant select, insert, update, delete on Sklad to director1
grant select, insert, update, delete on Zamovlenni_Stravy to director1
grant select, insert, update, delete on Kliyenty to director1


--��������
grant select, insert, update, delete on Menyu to manager1
grant select on Personal to manager1
grant select, update, delete on Sklad to manager1
grant select, insert, update, delete on Zamovlenni_Stravy to manager1
grant select, update, delete on Kliyenty to manager1

deny insert, update, delete on Personal to manager1
deny insert on Sklad to manager1
deny insert on Kliyenty to manager1

--�����
grant select on Menyu to kyhar1
grant select on Sklad to kyhar1
grant select on Zamovlenni_Stravy to kyhar1
grant select on Kliyenty to kyhar1

deny insert, update, delete on Menyu to kyhar1
deny select, insert, update, delete on Personal to kyhar1
deny insert, update, delete on Sklad to kyhar1
deny insert, update, delete on Zamovlenni_Stravy to kyhar1
deny insert, update, delete on Kliyenty to kyhar1

--��������
grant select, insert on Menyu to oficiant1
grant select on Sklad to oficiant1
grant select, insert on Zamovlenni_Stravy to oficiant1
grant select on Kliyenty to oficiant1

deny update, delete on Menyu to oficiant1
deny select, insert, update, delete on Personal to oficiant1
deny insert, update, delete on Sklad to oficiant1
deny update, delete on Zamovlenni_Stravy to oficiant1
deny insert, update, delete on Kliyenty to oficiant1

--������
grant select on Menyu to barmen1
grant select on Sklad to barmen1
grant select on Zamovlenni_Stravy to barmen1
grant select on Kliyenty to barmen1

deny insert, update, delete on Menyu to barmen1
deny select, insert, update, delete on Personal to barmen1
deny insert, update, delete on Sklad to barmen1
deny insert, update, delete on Zamovlenni_Stravy to barmen1
deny insert, update, delete on Kliyenty to barmen1

--��������
EXECUTE AS LOGIN = 'kyhar';

INSERT INTO Zamovlenni_Stravy (ID_Kliyenta, ID_Stravy, ID_Personalu, Data_Zamovlennya, Zahalna_Suma, Kilkist)
VALUES (1, 1, 1, '2024-01-05 21:00:00', 200.00, 2);

REVERT;

DROP CERTIFICATE DB_SERT;
DROP DATABASE ENCRYPTION KEY;
--------------����������----------------------
-- ��������� ����� �� ����������� 
CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'kyrsova';
CREATE CERTIFICATE DB_SERT WITH SUBJECT = 'Sertuficat';

-- ��������� ��������� ���������� (TDE)
USE Restaurant;
CREATE DATABASE ENCRYPTION KEY
WITH ALGORITHM = AES_256
ENCRYPTION BY SERVER CERTIFICATE DB_SERT;

-- �������� ��������� ����� �� �����������
BACKUP CERTIFICATE DB_SERT TO FILE = 'C:\Sertuficat\DB_SERT.cer' WITH PRIVATE KEY (FILE = 'C:\Sertuficat\PrivateKey.pvk', ENCRYPTION BY PASSWORD = 'kyrsova');

--�������� ����� �� �����������
SELECT name FROM sys.certificates WHERE name = 'DB_SERT';
SELECT * FROM sys.symmetric_keys;


--��������� �������
CREATE INDEX IX_Nazva_Stravy ON Menyu(Nazva_Stravy);
CREATE INDEX IX_ID_Stravy ON Menyu(ID_Stravy);

--������� ��� ���������� �������
CREATE UNIQUE INDEX IX_Unique_Nazva_Stravy ON Menyu(Nazva_Stravy);	




SELECT * FROM Kliyenty;
SELECT * FROM Menyu;
SELECT * FROM Zamovlenni_Stravy;
SELECT * FROM Inhrediyentu;
SELECT * FROM Sklad;
SELECT * FROM Inhrediyentu_Stravu;
SELECT * FROM Postavki;
SELECT * FROM Personal;
SELECT * FROM Bronyuvannya;

delete from Menyu where ID_Stravy = 1 
delete from Zamovlenni_Stravy